<?php
    require('animal.php'); 
    require('frog.php');
    require('ape.php');

    $animal = new Animal("Sheep");
    echo "Name : " . $animal->name . "<br>";
    echo "Legs : " . $animal->legs . "<br>";
    echo "Cold Blood : " ; var_dump ($animal->cold_blooded) . "<br>";
    echo "<br>"; 
    echo "<br>";
    $frog = new Frog("buduk");
    echo "Name : " . $frog->name . "<br>";
    echo "Legs : " . $frog->legs . "<br>";
    echo "Cold Blood : " ; var_dump($frog->cold_blooded). "<br>";
    echo "<br>";
    echo "Bunyi : "  ; echo $frog->jump();
    echo "<br>";
    echo "<br>";
    $ape = new Ape("kera sakti");
    echo "Name : " . $ape->name . "<br>";
    echo "Legs : " . $ape->legs . "<br>";
    echo "Cold Blood : " ; var_dump($ape->cold_blooded). "<br>";
    echo "<br>";
    echo "Bunyi : " ; echo $ape->yell();
?>